import itertools
import re

class BookStore:
    def __init__(self):
        self.inventory = {
            "Flaubert": [("Bouvard et Pecuchet", 5)],
            "Proust": [("La recherche du temps perdu", 10)],
            "Orwell": [("1984", 3), ("La ferme des animaux", 1)]
        }

    def authors(self):
        """Return the list of authors"""
        return list(self.inventory.keys())

    def author(self, name):
        """
        Return the list of books from this author in inventory
        as a list of couples (title, number)
        """
        for key, value in self.inventory.items():
            if key == name:
                return value

    def search(self, partial_title):
        """
        Search if a given book is available from a partial title
        for all books matchin title returns how many books
        we have in stock and the full title.
        """
        books = list(itertools.chain.from_iterable(self.inventory.values()))
        return [book for book in books if book[0].find(partial_title) > -1]

    def sell(self, author, title):
        """
        Remove one book from inventory
        """
        books = self.inventory.get(author, [])

        for index, book in enumerate(books):
            if book[0] == title:
                return books.pop(index)
        raise LookupError('book 1984 by Orwell not found')

    def add_book(self, author, title, number=1):
        """Add one or several identical books to inventory"""
        books = self.inventory.get('author', [])
        if not books:
            self.inventory[author] = [(title, number)]
        else:
            books.append((title, number))
        return self.inventory

    @staticmethod
    def _title_match_proces(title1):
        """
        Perform pre-processing on a book title for comparison.
        """
        title1 = BookStore.remove_space(title1)
        title1 = BookStore.to_lower(title1)
        title1 = BookStore.remove_punctuation(title1)
        return title1

    @staticmethod
    def title_match(title1, title2):
        """This function should return true if title1 and title2 are identical 
        with the below allowances:
        - case is ignored
        (ie "le vieil homme et la mer" is the same as "Le Vieil Homme et la Mer")
        - multiple spaces are treated as a single space
        (ie "Le   Vieil   Homme et   la    Mer" is the same as "Le Vieil Homme et la Mer")
        - punctuation is non significant but is considered as a word separator
        (ie "2001 Odysée de l'espace" is the same as "2001, Odyssée de l'espace")
        - Accentuated characters are treated as equivalent to the matching non accentuated characters
        (ie "2001 Odysée de l'espace" is the same as "2001 Odyssee de l'espace")
        - the first word of the string can also be appended to the end of the string after a coma
        (ie: "Le Vieil Homme et la mer" is considered to be the same as "Vieil Homme et la Mer, Le")
        """
        title1 = BookStore._title_match_proces(title1)
        title2 = BookStore._title_match_proces(title2)

        title1 = [i for i in list(''.join(title1)) if i != ' ']
        title2 = [i for i in list(''.join(title2)) if i != ' ']

        if set(title1) == set(title2):
            return True
        return False

    @staticmethod
    def remove_space(title1):
        """remove leading space in string"""
        return " ".join(re.split("\s+", title1))

    @staticmethod
    def to_lower(title1):
        """convert text to lower text"""
        return title1.lower()

    @staticmethod
    def remove_punctuation(title1):
        """remove all punctuation"""
        punc = '''!()-[]{};:'"\,<>./?@#$%^&*_~'''
        for ele in title1:
            if ele in punc:
                title1 = title1.replace(ele, "")

        return title1
